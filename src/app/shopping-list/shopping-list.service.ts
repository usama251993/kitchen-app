import { Injectable, EventEmitter } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {

  changedIngredients = new EventEmitter<Ingredient[]>();

  ingredients: Ingredient[] = [
    new Ingredient("Hydrogen", 14),
    new Ingredient("Oxygen", 7),
    new Ingredient("Carbon", 6),
    new Ingredient("Nitrogen", 0)
  ];

  selectedIngredientEvent = new EventEmitter<Ingredient>();

  constructor() { }

  returnIngredients() {
    return this.ingredients.slice(0);
  }

  addIngredients(ingredientObject: Ingredient) {
    this.ingredients.push(ingredientObject);
    this.changedIngredients.emit(this.ingredients.slice(0));
  }

  addIngredientsToShoppingList(ingredientsArray: Ingredient[]) {
    this.ingredients.push(...ingredientsArray);
    this.changedIngredients.emit(this.ingredients.slice(0));
  }

  populateForm(ingredient: Ingredient) {
    this.selectedIngredientEvent.emit(ingredient);
  }

}
