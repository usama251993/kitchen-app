import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { Ingredient } from 'src/app/shared/ingredient.model';
import { ShoppingListService } from '../shopping-list.service';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
  @ViewChild("inputName") inputNameReference: ElementRef;
  @ViewChild("inputAmount") inputAmountReference: ElementRef;

  selectedIngredient: Ingredient;

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
    this.shoppingListService.selectedIngredientEvent.subscribe((ingredient: Ingredient) => {
      this.selectedIngredient = ingredient;
    });
  }

  ingredientAddClicked() {
    let ingredientName = this.inputNameReference.nativeElement.value;
    let ingredientAmount = this.inputAmountReference.nativeElement.value;
    let ingredientObject = new Ingredient(ingredientName, ingredientAmount);
    this.shoppingListService.addIngredients(ingredientObject);
  }

}
