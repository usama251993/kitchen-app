import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ServerStatusComponent } from './server-status/server-status.component';
import { ServerFormComponent } from './server-status/server-form/server-form.component';
import { ServerDisplayComponent } from './server-status/server-display/server-display.component';
import { ServerInstructionComponent } from './server-status/server-instruction/server-instruction.component';
import { RecipeComponent } from './recipe/recipe.component';
import { RecipeHeaderComponent } from './recipe-header/recipe-header.component';
import { RecipeListComponent } from './recipe/recipe-list/recipe-list.component';
import { RecipeItemComponent } from './recipe/recipe-list/recipe-item/recipe-item.component';
import { RecipeDetailComponent } from './recipe/recipe-detail/recipe-detail.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { ShoppingEditComponent } from './shopping-list/shopping-edit/shopping-edit.component';
import { DropdownDirective } from './shared/directives/dropdown.directive';
import { ShoppingListService } from './shopping-list/shopping-list.service';
import { AppRoutingModule } from './app-routing.module';
import { RecipeStartComponent } from './recipe/recipe-start/recipe-start.component';
import { RecipeEditComponent } from './recipe/recipe-edit/recipe-edit.component';
import { InvalidRouteComponent } from './invalid-route/invalid-route.component';


@NgModule({
  declarations: [
    AppComponent,
    ServerStatusComponent,
    ServerFormComponent,
    ServerDisplayComponent,
    ServerInstructionComponent,
    RecipeComponent,
    RecipeHeaderComponent,
    RecipeListComponent,
    RecipeItemComponent,
    RecipeDetailComponent,
    ShoppingListComponent,
    ShoppingEditComponent,
    DropdownDirective,
    RecipeStartComponent,
    RecipeEditComponent,
    InvalidRouteComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [ShoppingListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
