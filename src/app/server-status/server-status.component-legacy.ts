import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-server-status',
  templateUrl: './server-status.component.html',
  styleUrls: ['./server-status.component.css']
})

export class ServerStatusComponent implements OnInit {

  domStrings: any = {
    instructionElement: {
      instructionHeading: "Instructions",
      instructionsArray: [
        "You can add a new server name using the form below",
        "Servers added by you will be enlisted below the form in tabular format",
        "You cannot add two servers with the same name"
      ]
    },
    formElements: {
      labelStrings: {
        serverName: "Name of the server"
      },
      buttonStrings: {
        addNewServer: "Add Server",
        nameRequired: "Server name is required"
      }
    },
    responseStrings: {
      nothingAdded: "No servers added yet",
      serverAdded: " added!",
      serverAlreadyExists: " already exists"
    },
  };
  domBehaviour: any = {
    allowNewServer: false,
    buttonText: "",
    statusText: "",
    isServerAvailable: false
  };
  serverNameHolder: string = "";
  userServers: any = [];

  constructor() { }

  ngOnInit() {
    this.domBehaviour.allowNewServer = false;
    this.domBehaviour.buttonText = this.domStrings.formElements.buttonStrings.nameRequired;
    this.domBehaviour.statusText = this.domStrings.responseStrings.nothingAdded;
    this.domBehaviour.isServerAvailable = (this.userServers.length === 0) ? false : true;
  }

  getServerName(eventData: Event) {
    if ((eventData.target as HTMLInputElement).value.length === 0) {
      this.domBehaviour.allowNewServer = false;
      this.domBehaviour.buttonText = this.domStrings.formElements.buttonStrings.nameRequired;
    } else {
      this.serverNameHolder = (eventData.target as HTMLInputElement).value;
      let serverName = this.serverNameHolder;
      let isServerRepeated = false;
      this.userServers.forEach((userServer: any) => {
        if (userServer.serverName === serverName) isServerRepeated = true;
      });
      if (isServerRepeated) {
        this.domBehaviour.allowNewServer = false;
        this.domBehaviour.statusText = this.serverNameHolder + this.domStrings.responseStrings.serverAlreadyExists;
        this.domBehaviour.buttonText = this.serverNameHolder + this.domStrings.responseStrings.serverAlreadyExists;
      } else {
        this.domBehaviour.allowNewServer = true;
        this.domBehaviour.buttonText = this.domStrings.formElements.buttonStrings.addNewServer;
      }
    }
  }
  addNewServer() {
    this.domBehaviour.isServerAvailable = true;
    let tempObject = {
      serverName: this.serverNameHolder,
      serverStatus: (Math.random() > 0.25) ? "online" : "offline"
    };
    this.userServers.push(tempObject);
    this.domBehaviour.statusText = this.serverNameHolder + this.domStrings.responseStrings.serverAdded;
    this.domBehaviour.allowNewServer = false;
    this.domBehaviour.buttonText = this.serverNameHolder + this.domStrings.responseStrings.serverAdded;
  }

}
