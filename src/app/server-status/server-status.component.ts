import { Component, OnInit, ViewEncapsulation, OnChanges, SimpleChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-server-status',
  templateUrl: './server-status.component.html',
  styleUrls: ['./server-status.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class ServerStatusComponent implements OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {

  serverElements: any = {
    genericStrings: {
      serverString: "server",
      blueprintString: "blueprint"
    },
    domStrings: {
      instructionElement: {
        instructionHeading: "Instructions",
        instructionsArray: [
          "You can add a new server name using the form below",
          "Servers added by you will be enlisted below the form in tabular format",
          "You cannot add two servers with the same name"
        ]
      },
      formElements: {
        labelStrings: {
          serverName: "Name of the server"
        },
        buttonStrings: {
          addNewServer: "Add Server",
          addNewServerBlueprint: "Add Blueprint",
          nameRequired: "Server name is required"
        }
      },
      responseStrings: {
        appReady: "Ready to go!",
        serverStrings: {
          nothingAdded: "No servers added yet",
          serverAdded: " added!",
          serverAlreadyExists: " already exists"
        },
      },
    },
    domBehaviour: {
      allowNewServer: false,
      buttonText: "",
      statusText: "",
      isServerAvailable: false
    },
    userServers: []
  };

  constructor() {
    console.log("Studying LifeCycle Hooks");
    console.log("constructor called");
  }

  ngOnInit() {
    this.serverElements.domBehaviour.
      allowNewServer = false;
    this.serverElements.domBehaviour.
      buttonText = this.serverElements.domStrings.formElements.buttonStrings.nameRequired;
    this.serverElements.domBehaviour.
      statusText = this.serverElements.domStrings.responseStrings.appReady;
    this.serverElements.domBehaviour.
      isServerAvailable = (this.serverElements.userServers.length === 0) ? false : true;
    console.log("ngOnInit called");
  }
  ngAfterContentInit() {
    console.log("ngAfterContentInit called");
  }

  ngOnChanges(changeObject: SimpleChanges) {
    console.log("ngOnChanges called");
    console.log(changeObject);
  }

  ngDoCheck() {
    console.log("ngDoCheck called");
  }

  ngAfterContentChecked() {
    console.log("ngAfterContentChecked called");
  }

  ngAfterViewInit() {
    console.log("ngAfterViewInit called");
  }

  ngAfterViewChecked() {
    console.log("ngAfterViewChecked called");
  }

  ngOnDestroy() {
    console.log("ngOnDestroy called");
  }

  serverCreateEvent(serverData: { serverName: string, serverStatus: string }) {
    let serverAlreadyExists = false;
    let tempObject = {
      serverName: serverData.serverName,
      serverStatus: serverData.serverStatus
    };

    this.serverElements.userServers.forEach((serverObject: { serverName: string, serverStatus: string }) => {
      if (serverObject.serverName === tempObject.serverName) serverAlreadyExists = true;
    });

    if (serverAlreadyExists) {
      this.serverElements.domBehaviour.
        statusText = tempObject.serverName + this.serverElements.domStrings.responseStrings.serverStrings.serverAlreadyExists;
    } else {
      this.serverElements.userServers.push(tempObject);
      this.serverElements.domBehaviour.
        statusText = tempObject.serverName + this.serverElements.domStrings.responseStrings.serverStrings.serverAdded;
      this.serverElements.domBehaviour.
        buttonText = tempObject.serverName + this.serverElements.domStrings.responseStrings.serverStrings.serverAdded;
      this.serverElements.domBehaviour.
        allowNewServer = false;
    }
  }
}

