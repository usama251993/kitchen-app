import { Component, OnInit, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-server-display',
  templateUrl: './server-display.component.html',
  styleUrls: ['./server-display.component.css']
})
export class ServerDisplayComponent implements OnInit {

  @Input("serverDisplay") serverElements: any;

  constructor() { }

  ngOnInit() { }

}
