import { Component, OnInit, Input, EventEmitter, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-server-form',
  templateUrl: './server-form.component.html',
  styleUrls: ['./server-form.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ServerFormComponent implements OnInit {

  @Input("serverForm") serverElements: any;

  @Output() serverCreateEvent = new EventEmitter<{
    serverName: string,
    serverStatus: string
  }>();

  serverName: string = "";

  constructor() { }

  ngOnInit() { }

  getServerName(eventData: Event) {
    let inputString = (<HTMLInputElement>eventData.target).value.trim();
    if (inputString.length === 0) {
      this.serverElements.domBehaviour.
        allowNewServer = false;
      this.serverElements.domBehaviour.
        statusText = this.serverElements.domStrings.formElements.buttonStrings.nameRequired;
      this.serverElements.domBehaviour.
        buttonText = this.serverElements.domStrings.formElements.buttonStrings.nameRequired;
    } else {
      let serverAlreadyExists = false;
      this.serverElements.userServers.forEach((serverObject: { serverType: string, serverName: string, serverStatus: string }) => {
        if (serverObject.serverName === inputString) serverAlreadyExists = true;
      });
      if (serverAlreadyExists) {
        this.serverElements.domBehaviour.
          allowNewServer = false;
        this.serverElements.domBehaviour.
          statusText = inputString + this.serverElements.domStrings.responseStrings.serverStrings.serverAlreadyExists;
        this.serverElements.domBehaviour.
          buttonText = inputString + this.serverElements.domStrings.responseStrings.serverStrings.serverAlreadyExists;
      } else {
        this.serverElements.domBehaviour.
          allowNewServer = true;
        this.serverElements.domBehaviour.
          buttonText = this.serverElements.domStrings.formElements.buttonStrings.addNewServer;
        this.serverElements.domBehaviour.
          statusText = this.serverElements.domStrings.responseStrings.appReady;
      }
    }
  }

  addNewServer() {
    this.serverCreateEvent.emit({
      serverName: this.serverName,
      serverStatus: (Math.random() > 0.7) ? "online" : "offline"
    });
  }
}
