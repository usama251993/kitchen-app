import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerInstructionComponent } from './server-instruction.component';

describe('ServerInstructionComponent', () => {
  let component: ServerInstructionComponent;
  let fixture: ComponentFixture<ServerInstructionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServerInstructionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerInstructionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
