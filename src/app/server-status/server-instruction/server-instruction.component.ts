import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
// import { ServerStatusComponent } from '../server-status.component';

@Component({
  selector: 'app-server-instruction',
  templateUrl: './server-instruction.component.html',
  styleUrls: ['./server-instruction.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ServerInstructionComponent implements OnInit {

  @Input("serverInstruction") serverElements: any;

  constructor() { }

  ngOnInit() {
  }

}
