import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {

  selectedRecipe: Recipe;
  id: number;

  constructor(private recipeService: RecipeService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.selectedRecipe = this.recipeService.getRecipe(params["id"]);
      this.id = params["id"];
    });
  }

  addIngredientsToList() {
    this.recipeService.addIngredientsForShopping(this.selectedRecipe.ingredientsArray);
  }

  editRecipeClicked() {
    this.router.navigate(["../", this.id, "edit"], { relativeTo: this.route });
    this.recipeService.populateRecipeForm(this.selectedRecipe);
  }

  deleteRecipeClicked() {
    this.router.navigate(["../"], { relativeTo: this.route });
    this.recipeService.deleteRecipe(this.id);
  }

}
