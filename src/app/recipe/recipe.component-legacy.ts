import { Component, OnInit } from '@angular/core';
// import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css'],

  // Required for Method 1
  // providers: [RecipeService]
  providers: []
})

export class RecipeComponent implements OnInit {

  domStrings: any = {
    instructionsArray: [
      "Try to add some ingredients using the input textbox and also specify the quantity",
      "You cannot add the ingredient with same name"
    ],
    labelStrings: {
      ingredientName: "Name of the ingredient",
      ingredientQuantity: "Quantity"
    },
    buttonStrings: {
      addNewIngredient: "Add Ingredient",
      nameRequired: "Ingredient name is required",
      quantityRequired: "Ingredient Quantity is required",
      allRequired: "Ingredient name as well as quantity are required"
    },
    responseStrings: {
      nothingAdded: "No ingredients added yet",
      ingredientAlreadyExists: " already exists",

    }
  };

  userIngredients: any = [];

  allowNewIngredient: boolean = false;
  isIngredientAdded: boolean = false;
  statusText: string = "";
  buttonText: any = {
    addButton: "",
    deleteButton: "Clear All"
  };

  constructor(
    // Method 1
    // Instantiate in constructor
    // private recipeService: RecipeService
  ) {

    this.statusText = this.domStrings.responseStrings.nothingAdded;
    // this.buttonText = this.domStrings.buttonStrings.allRequired;
    this.buttonText.addButton = this.domStrings.buttonStrings.addNewIngredient;
    this.allowNewIngredient = true;

  }

  ngOnInit() {
    // Method 1
    // Component Level
    // this.recipesArray = this.recipeService.getRecipes();
  }

  addNewIngredient() {
    let inputBoxes = window.document.querySelectorAll("input[type=\"text\"]");
    let ingredientObject = {
      name: (inputBoxes[0] as HTMLInputElement).value,
      quantity: (inputBoxes[1] as HTMLInputElement).value
    };
    this.userIngredients.push(ingredientObject);
  }

  validate(eventData: any) {
    let inputBox = eventData.target;
    if (inputBox.name.indexOf("ingredient-name")) {
      if (inputBox.value.length === 0) {
        if (/*check quantity*/ true) {
          this.buttonText.addButton = this.domStrings.buttonStrings.nameRequired;
        } else {
          this.buttonText.addButton = this.domStrings.buttonStrings.allRequired;
        }
      } else {
        if (/*check quantity*/ true) {
          this.buttonText.addButton = this.domStrings.buttonStrings.addNewIngredient;
          this.allowNewIngredient = true;
        } else {
          this.buttonText.addButton = this.domStrings.buttonStrings.quantityRequired;
        }
      }
    } else {
      if (inputBox.value.length === 0) {
        if (/*check name*/ true) {
          this.buttonText.addButton = this.domStrings.buttonStrings.nameRequired;
        } else {
          this.buttonText.addButton = this.domStrings.buttonStrings.allRequired;
        }
      } else {
        if (/*check name*/ true) {
          this.buttonText.addButton = this.domStrings.buttonStrings.addNewIngredient;
          this.allowNewIngredient = true;
        } else {
          this.buttonText.addButton = this.domStrings.buttonStrings.quantityRequired;
        }
      }
    }
  }

  inputIngredient(eventData: any) {

  }

}
