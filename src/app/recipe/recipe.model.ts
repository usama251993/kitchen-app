import { Ingredient } from '../shared/ingredient.model';

export class Recipe {
  constructor(public name: string,
    public description: string,
    public image: { path: string, altText: string },
    public ingredientsArray: Ingredient[]) {
  }
}