import { Injectable, EventEmitter } from '@angular/core';

import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';

@Injectable({
  providedIn: 'root'
})

export class RecipeService {

  recipeClickEvent = new EventEmitter<Recipe>();
  recipeCreateEvent = new EventEmitter();
  recipeSelectEvent = new EventEmitter<Recipe>();
  recipeEditEvent = new EventEmitter();
  recipeDeleteEvent = new EventEmitter();

  private recipesArray: Recipe[] = [
    new Recipe(
      "Water",
      "Hydroxylic Acid",
      {
        path: "../../assets/images/water.svg",
        altText: "H2O"
      }, [
        { name: "Hydrogen", quantity: 2 },
        { name: "Oxygen", quantity: 1 }
      ]),

    new Recipe(
      "Sugar",
      "D-Glucose",
      {
        path: "../../assets/images/sugar.svg",
        altText: "C6H12O6"
      }, [
        { name: "Carbon", quantity: 6 },
        { name: "Hydrogen", quantity: 12 },
        { name: "Oxygen", quantity: 6 }
      ]),

    new Recipe(
      "Beer",
      "Monoatomic Ethanol",
      {
        path: "../../assets/images/beer.svg",
        altText: "C2H5OH"
      }, [
        { name: "Carbon", quantity: 2 },
        { name: "Hydrogen", quantity: 6 },
        { name: "Oxygen", quantity: 1 }
      ]),

    // new Recipe("Chicken", "Valine",
    //   { path: "../assets/images/chicken.png", altText: "C5H11NO2" },
    //   [{ name: "Carbon", quantity: 5 }, { name: "Hydrogen", quantity: 11 },
    //   { name: "Oxygen", quantity: 2 }, { name: "Nitrogen", quantity: 1 }])
  ];


  constructor(private shoppingListService: ShoppingListService) { }

  returnRecipe() {
    return this.recipesArray.slice(0); // Return a copied array
  }

  getRecipe(index: number) {
    return this.recipesArray[index];
  }

  // Called in recipe-edit.component.ts
  createNewRecipe(formData: FormData) {
    let newRecipe = new Recipe(
      formData["name"],
      formData["description"],
      { path: "../../assets/images/food.svg", altText: "Food!!" },
      formData["ingredientsArray"].slice(0));
    this.recipesArray.push(newRecipe);
    this.recipeCreateEvent.emit();
  }

  // Called in recipe-edit.component.ts after formSubmit
  editRecipe(formData: FormData, recipeId: number) {
    let editedRecipe = new Recipe(
      formData["name"],
      formData["description"],
      {
        path: formData["image"]["path"],
        altText: formData["image"]["altText"]
      },
      formData["ingredientsArray"].slice(0)
    );
    this.recipesArray.splice(+recipeId, 1, editedRecipe);
    this.recipeEditEvent.emit();
  }

  deleteRecipe(recipeIndex: number) {
    this.recipesArray.splice(recipeIndex, 1);
    this.recipeDeleteEvent.emit();
  }

  addIngredientsForShopping(ingredientsArray: Ingredient[]) {
    this.shoppingListService.addIngredientsToShoppingList(ingredientsArray);
  }

  // Called in recipe-edit.component.ts
  populateRecipeForm(recipe: Recipe) {
    this.recipeSelectEvent.emit(recipe);
  }

}
