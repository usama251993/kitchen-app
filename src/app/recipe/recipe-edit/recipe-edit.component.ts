import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormBuilder, Validators, FormArray, FormGroup, FormControl, AbstractControl } from '@angular/forms';

import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';
import { Ingredient } from 'src/app/shared/ingredient.model';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {

  // To fetch recipe details while editing
  newRecipe: boolean;
  imageName: string;
  selectedRecipe: Recipe;

  // Temporary properties of class to create or edit the recipe
  recipeIngredients: Ingredient[] = [];

  // Initialized in ngOnInit
  recipeForm: FormGroup = this.formBuilder.group({});
  singleIngredient: FormArray = this.formBuilder.array([]);

  id: number;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private recipeService: RecipeService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {

    // Route Subscription
    this.route.params.subscribe((params: Params) => {

      this.recipeForm = this.formBuilder.group({
        name: ["", [Validators.required]],
        description: [""],
        image: this.formBuilder.group({
          path: [""],
          altText: [""]
        }),
        ingredientsArray: this.singleIngredient
      });

      // Check if the recipe clicked exists
      this.newRecipe = (params["id"] === undefined) ? true : false;

      // if the recipe exists
      if (!this.newRecipe) {
        this.id = params["id"];
        this.selectedRecipe = this.recipeService.getRecipe(params["id"]);
        this.selectedRecipe.ingredientsArray.forEach((ingredient) => {
          this.ingredients.controls.push(this.formBuilder.group({
            name: ["", [Validators.required]],
            quantity: ["", [Validators.required]]
          }));
        });

        for (let key in this.recipeForm.value) {
          this.recipeForm.get(key).setValue(this.selectedRecipe[key]);
        }

        // this.imageName = this.selectedRecipe.image.path.split("/").reduce((firstSplitElement, lastSplitElement) => lastSplitElement);
        // this.recipeIngredients = this.selectedRecipe.ingredientsArray.slice(0);

      } else { }

      this.ingredients.push(this.formBuilder.group({
        name: ["", [Validators.required]],
        quantity: ["", [Validators.required]]
      }));

    });

  }

  get ingredients() {
    return this.recipeForm.get("ingredientsArray") as FormArray;
  }

  addNewRow() {
    this.ingredients.push(this.formBuilder.group({
      name: ["", [Validators.required]],
      quantity: ["", [Validators.required]]
    }));
  }

  deleteRow(index: number) {
    this.ingredients.removeAt(index);
  }

  formSubmit() {
    if (this.newRecipe) {
      this.router.navigate(["../"], { relativeTo: this.route })
      this.recipeService.createNewRecipe(this.recipeForm.value);
    } else {
      this.router.navigate(["../"], { relativeTo: this.route })
      this.recipeService.editRecipe(this.recipeForm.value, this.id);
    }
  }

}
