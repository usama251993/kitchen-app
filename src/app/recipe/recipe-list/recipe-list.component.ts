import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipesArray: Recipe[];

  constructor(private recipeService: RecipeService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.recipesArray = this.recipeService.returnRecipe();

    this.recipeService.recipeCreateEvent.subscribe(() => {
      this.recipesArray = this.recipeService.returnRecipe();
    });

    this.recipeService.recipeEditEvent.subscribe(() => {
      this.recipesArray = this.recipeService.returnRecipe();
    });

    this.recipeService.recipeDeleteEvent.subscribe(() => {
      this.recipesArray = this.recipeService.returnRecipe();
    });
  }

  newRecipeClicked() {
    this.router.navigate(["new"], { relativeTo: this.route });
  }

}
