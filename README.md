# KitchenApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Keeping a track of features 
## Alt 1 + 7 + 9 = │
## Alt 1 + 9 + 2 = └
## Alt 1 + 9 + 4 = ┬
## Alt 1 + 9 + 5 = ├
## Alt 1 + 9 + 6 = ─
## Alt 2 + 1 + 8 = ┌

## Commit #5: Added Beer and disabled all the semi-non-functional buttons
┌─ What's working
│
├──┬ Navigation using navbar
│  ├── Recipe Component
│  └── Shopping List Component
│
├──┬ Fetching of data from service to populate
│  ├── Recipe List
│  └── Shopping List
│
├─── Display details of a recipe when clicked
│
├─── Edit Recipe button populates the edit recipe form
│
├─── New Recipe Button displays form to enter fields of a new recipe
│
├─── Fetching of data from service to populate Shopping List
│
└─── The fields of shopping list form are populated when an item is clicked

────────────────────────────────────────────────────────────────

┌─ What's not working
│
├──┬ Manage
│  ├── Save Data
│  └── Fetch Data
│
├─── Unable to create a New Recipe
│
├─── Unable to Edit Recipe details on the Home Page
│
├──┬ Manage Recipe
│  ├── Add Recipe to Shopping List
│  └── Delete Recipe Here
│
└──┬ Unable to perform Shopping List operations
   ├── Add Ingredients to Shopping List directly
   └── Add Ingredients to Shopping List from recipe

## Commit #6: Ingredients and Manage Recipe modified and form population done
┌─ What's working
│
├──┬ Navigation using navbar
│  ├── Recipe Component
│  └── Shopping List Component
│
├──┬ Fetching of data from service to populate
│  ├── Recipe List
│  └── Shopping List
│
├──┬ Display details of a recipe when clicked
│  ├── Ingredients form group modified to tablular view
│  └── Manage Recipe is now split in three buttons
│
├─── New Recipe Button displays form to enter fields of a new recipe
│
├─── Edit Recipe button populates the edit recipe form
│
├─── Fetching of data from service to populate Shopping List
│
└─── The fields of shopping list form are populated when an item is clicked

────────────────────────────────────────────────────────────────

┌─ What's not working
│
├──┬ Manage
│  ├── Save Data
│  └── Fetch Data
│
├─── Unable to create a New Recipe - Pending form validation
│
├─── Unable to Edit Recipe details on the Home Page - Pending form validation
│
├──┬ Manage Recipe
│  ├── Add Recipe to Shopping List
│  └── Delete Recipe Here
│
└──┬ Unable to perform Shopping List operations
   ├── Add Ingredients to Shopping List directly
   └── Add Ingredients to Shopping List from recipe

## Commit #7: New and Edit Forms working perfectly!

┌─ What's working
│
├──┬ Navigation using navbar
│  ├── Recipe Component
│  └── Shopping List Component
│
├──┬ Fetching of data from service to populate
│  ├── Recipe List
│  └── Shopping List
│
├─── User is able to create a New Recipe successfully
│
├─── User is able to Edit an existing Recipe successfully
│
├─── Fetching of data from service to populate Shopping List
│
└─── The fields of shopping list form are populated when an item is clicked

────────────────────────────────────────────────────────────────

┌─ What's not working
│
├──┬ Manage
│  ├── Save Data
│  └── Fetch Data
│
├─── Create a New Recipe - Image acquisition not designed
│
├─── Edit Recipe - Image acquisition not designed
│
├──┬ Manage Recipe
│  ├── Add Recipe to Shopping List
│  └── Delete Recipe Here
│
└──┬ Unable to perform Shopping List operations
   ├── Add Ingredients to Shopping List directly
   └── Add Ingredients to Shopping List from recipe